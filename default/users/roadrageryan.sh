#!/bin/bash

# Create user
useradd -m -s /bin/bash -U -G sudo -c "Road Rage Ryan" roadrageryan

# Force users to change password
chage -d 0 roadrageryan

# Install SSH keys
echo 'ssh-rsa ...' > /home/roadrageryan/.ssh/authorized_keys
