#!/bin/bash

# Update the system
apt-get -y update
apt-get -y dist-upgrade
apt-get -y autoclean
apt-get -y autoremove

# Check out the automation data


# Update skeleton home directory
mkdir -p /etc/skel/.ssh

# Create users
useradd -m -s /bin/bash -U -G sudo -c "Ryan Pratt" rmpratt1
useradd -m -s /bin/bash -U -G sudo -c "April Pratt" n1xri

# Force users to change password
chage -d 0 rmpratt1
chage -d 0 n1xri

# Install SSH keys
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8Fsaku70GSb+BWMjGgJ26I88kw3DNyWvkjN4drBDGXlZcJ4KkF1VCr+bVi7a0z+Oh6c3NWsxsDrsfGaf/G5+X97RxXIYwpX273/neIjS1zfFA6uhGRF1DeSojcHCPm4rBljH+RIGVp5Zl+Or2Q0O1RMokh6k7aLTpYq/ghyRTLEcgcCI/PK4rznCoC962t0eYpOoLaa0yBZW+hXFQ54NmaKGmC9wLFFwX5SxQagBOUL5Omiyej4tJAl8sgBP8mng6XJ35iOrkp4NaCBsRKSc2R723VZDAR3wcBQ0ZD5lB9dmAgfyxk0gaKKG/5oWH4l6gIthaTYy+TyiqghwncGm9 rmpratt1@Ryans-MacBook-Air.local' > /home/rmpratt1/.ssh/authorized_keys
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbfAMHVMaOyORppNRXmdU0kUe+eB2HBJDyFatYn7ZwcrPdIEL6AGxz9WvOJM/KZ94HUW7RBQzn9Y0Qnm0AlaIM8lvjYjcYtUIovmxTF7Zr4cCmarkCodIt5xtJ5RBsXePZSWdOsc1IJ1bpAH7fnHSTDFeVXN5WgZu3cR6u67paC8HRBAPS+vjgB3z5MfnTvKwLnwv4DdthTVprSbihkBMeJnT69efJLKpQg/MtBQo3m3TKtF8kS2s+imsKIrnBeILTFLXI9tjri/WGQk5qM6kX7PWBmjhtsIPc1HYtGNeiQkOLl+jpFtLWBiQH0LQ3XqlzeQIdLfEkXUGul0ZgbE+d n1xri@Aprils-MacBook-Air.local' > /home/n1xri/.ssh/authorized_keys


