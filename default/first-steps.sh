#!/bin/bash
#
# Defualt initial steps for all systems

# Update the system
apt-get -y update
apt-get -y dist-upgrade
apt-get -y autoclean
apt-get -y autoremove

# Update skeleton home directory
mkdir -p /etc/skel/.ssh
